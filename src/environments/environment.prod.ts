export const environment = {
  production: true,
  server: 'https://88insurtech-gi-sandbox.sg.ebaocloud.com/',
  cas: 'https://sandbox.s.auth.ebaocloud.com/cas/',
  container: 'https://sandbox.gw.sg.ebaocloud.com/'
};
