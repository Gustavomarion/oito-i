import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  public navOpen: boolean;
  public content = 'nav';
  
  constructor() { }

  public toggleNav(content: any = 'nav', close: boolean = true) {
    if (close) {
        this.navOpen = this.navOpen ? false : true;
    }
    if (this.content) {
        this.content = content;
    }
}


public closeNav(e) {
    // console.log(e.target);
    if (e.target.id === 'menu') {
        this.toggleNav();
    }
}

  ngOnInit() {
  }

}