export class Buy {
    policyInfo: {
        ProductCode: string,
        ProductVersion: string,
        ProposalDate: any,
        EffectiveDate: any,
        ExpiryDate: any,
        PremiumCurrencyCode: string,
        SiCurrencyCode: string,
        AgentCode: string,
        PolicyPaymentInfoList: [
            {
                PayModeCode: string,
                IsInstallment: string,
                InstallmentType: string
            }
        ],
        PolicyCustomerList: [
            {
                CustomerName: string,
                DateOfBirth: any,
                IdType: string,
                IdNo: string,
                City: string,
                State: string,
                Mobile: string,
                Email: string,
                PostCode: string,
                Address: string,
                AddressNumber: string,
                AddressComplement: string,
                Suburb: string,
                Gender: string,
                CustomerType: string,
                IsPolicyHolder: string,
                IsInsured: string
            }
        ],
        PolicyLobList: [
            {
                PolicyRiskList: [
                    {
                        DateofPurchase: any,
                        OneYearOlder: string,
                        EquipmentType: string,
                        IMEI: number,
                        InvoiceNumber: string,
                        Make: string,
                        OtherMake: string,
                        MarketValue: number,
                        Model: string,
                        OtherModel: string,
                        PhoneNo: string,
                        PlanName: string,
                        voucher: string,
                        PolicyCoverageList: [
                            {
                                ProductElementCode: string
                            },
                            {
                                ProductElementCode: string
                            }
                        ],
                        ProductElementCode: string
                    }
                ],
                ProductCode: string
            }
        ]
    };
    paymentInfo: {
        CreditCardNo: string,
        CreditCardCvv: string,
        CardholdersExpirationDate: string,
        CardholdersName: string
    }

    constructor(){
        {
            this.policyInfo = {
                ProductCode: "PEI",
                ProductVersion: "1.0",
                ProposalDate: "",
                EffectiveDate: "",
                ExpiryDate: "",
                PremiumCurrencyCode: "BRL",
                SiCurrencyCode: "BRL",
                AgentCode: "querocomprar",
                PolicyPaymentInfoList: [
                    {
                        PayModeCode: "30",
                        IsInstallment: "Y",
                        InstallmentType: "1"
                    }
                ],
                PolicyCustomerList: [
                    {
                        CustomerName: "Tiago Ravagnolli",
                        DateOfBirth: "1982-03-09",
                        IdType: "1",
                        IdNo: "29477024848",
                        City: "3550308",
                        State: "35",
                        Mobile: "11941225702",
                        Email: "tiagoravagnolli@gmail.com",
                        PostCode: "04750001",
                        Address: "Rua Doutor Antonio Bento",
                        AddressNumber: "504",
                        AddressComplement: "Apto 194 B",
                        Suburb: "Alto da Boa Vista",
                        Gender: "1",
                        CustomerType: "1",
                        IsPolicyHolder: "Y",
                        IsInsured: "N"
                    }
                ],
                PolicyLobList: [
                    {
                        PolicyRiskList: [
                            {
                                DateofPurchase: "2019-07-03",
                                OneYearOlder: "true",
                                EquipmentType: "1",
                                IMEI: 1238,
                                InvoiceNumber: "132213",
                                Make: "2",
                                OtherMake: "OTHER MAKE",
                                MarketValue: null,
                                Model: "12",
                                OtherModel: "OTHER MODEL",
                                PhoneNo: "11941225702",
                                PlanName: "2",
                                voucher: "EBAO-VALID",
                                PolicyCoverageList: [
                                    {
                                        ProductElementCode: "C101112"
                                    },
                                    {
                                        ProductElementCode: "C101113"
                                    }
                                ],
                                ProductElementCode: "R10062"
                            }
                        ],
                        ProductCode: "PEI"
                    }
                ]
            };
            this.paymentInfo = {
                CreditCardNo: "",
                CreditCardCvv: "",
                CardholdersExpirationDate: "",
                CardholdersName: ""
            }
        }
    }
}