import { Injectable } from '@angular/core';
import { RequestService } from '../../services/request.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BuyService extends RequestService {
    public container: string = environment.container;
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }
}