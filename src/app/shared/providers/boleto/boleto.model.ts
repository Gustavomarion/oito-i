export class Boleto {

    amount: number;
    payment_method: string;
    customer: {
        name: string,
        type: string,
        country: string,
        email: string,
        documents: [
            {
                type: string,
                number: string
            }
        ],
        phone_numbers: [
            string
        ],
        birthday: string,
        external_id: string
    };
    billing: {
        name: string,
        address: {
            country: string,
            state: string,
            city: string,
            neighborhood: string,
            street: string,
            street_number: string,
            zipcode: string
        }
    };
    items: [
        {
            id: string,
            title: string,
            unit_price: string,
            quantity: number,
            tangible: boolean
        }
    ];
    async: boolean;
    postback_url: string;
    metadata: {
        ProposalNo: string,
        coupon: string
    }

    constructor() {

        this.amount = 666;
        this.payment_method = "boleto";
        this.customer = {
            name: "Niedson ",
            type: "individual",
            country: "br",
            email: "niedson.araujo@ebaotech.com",
            documents: [
                {
                    type: "cpf",
                    number: "36085504888"
                }
            ],
            phone_numbers: [
                "+5511941225702"
            ],
            birthday: "1982-03-09",
            external_id: "#1234567"
        },
            this.billing = {
                name: "data.customer.name",
                address: {
                    country: "br",
                    state: "SP",
                    city: "São Paulo",
                    neighborhood: "123",
                    street: "4342 3k4n3",
                    street_number: "1213",
                    zipcode: "05687010"
                }
            },
            this.items = [
                {
                    id: "PPEI20200000000108",
                    title: "Proteção para celular",
                    unit_price: "666",
                    quantity: 1,
                    tangible: false
                }
            ],
            this.async = false,
            this.postback_url = "https://sandbox.gw.sg.ebaocloud.com/88insurtech/v1/pagarme/postback",
            this.metadata = {
                ProposalNo: "PPEI20200000000108",
                coupon: ""
            }
    }
}