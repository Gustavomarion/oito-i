export class Quick {
    ProductCode: string;
    ProductVersion: string;
    ProposalDate: any;
    EffectiveDate: any;
    ExpiryDate: any;
    AgentCode: string;
    PolicyPaymentInfoList: [
        {
            PayModeCode: string,
            IsInstallment: string,
            InstallmentType: string
        }
    ];
    PolicyCustomerList: [
        {
            CustomerName: string,
            Mobile: string,
            Email: string,
            IdNo: string,
            CustomerType: string,
            IdType: string,
            DateOfBirth: any,
            PostCode: string,
            Address: string,
            AddressNumber: string,
            AddressComplement: string,
            Suburb: string,
            State: string,
            City: string,
            IsPolicyHolder: string
        }
    ];
    PolicyLobList: [
        {
            PolicyRiskList: [
                {
                    DateofPurchase: any,
                    EquipmentType: string,
                    MarketValue: string,
                    InvoiceNumber: string,
                    IMEI: string,
                    Make: string,
                    Model: string,
                    OtherMake: string,
                    OtherModel: string,
                    OneYearOlder: string,
                    PolicyCoverageList: [
                        {
                            ProductElementCode: string,
                            ServiceProviderCode: string
                        }
                    ],
                    ProductElementCode: string,
                    PlanName: number
                }
            ]
            ProductCode: string
        }
    ]

    constructor() {
        this.ProductCode = "PEI";
        this.ProductVersion = "1.0";
        this.ProposalDate = "";
        this.EffectiveDate = "";
        this.ExpiryDate = "";
        this.AgentCode = "master";
        this.PolicyPaymentInfoList = [
            {
                PayModeCode: "30",
                IsInstallment: "Y",
                InstallmentType: "1"
            }
        ];
        this.PolicyCustomerList = [
            {
                CustomerName: "",
                Mobile: "",
                Email: "",
                IdNo: "",
                CustomerType: "1",
                IdType: "1",
                DateOfBirth: "",
                PostCode: "",
                Address: "",
                AddressNumber: "",
                AddressComplement: "",
                Suburb: "",
                State: "",
                City: "",
                IsPolicyHolder: "Y"
            }
        ];
        this.PolicyLobList = [
            {
                PolicyRiskList: [
                    {
                        DateofPurchase: '2018-02-21T00:00:00',
                        EquipmentType: '',
                        MarketValue: '',
                        InvoiceNumber: "",
                        IMEI: "",
                        Make: "",
                        Model: "",
                        OtherMake: "",
                        OtherModel: "",
                        OneYearOlder: "true",
                        PolicyCoverageList: [
                            {
                                ProductElementCode: "C101112",
                                ServiceProviderCode: "88i"
                            }
                        ],
                        ProductElementCode: "R10062",
                        PlanName: 1
                    }
                ],
                ProductCode: 'PEI'
            }
        ]
    }
}