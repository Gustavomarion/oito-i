import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  
  constructor(
    protected http: HttpClient
  ) { }

  sendGET(url: string, uri: string) {

    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers = headers.append(
      "Accept-Language", 'en'
    );
    headers = headers.append(
      "Authorization", 'Bearer ' + localStorage.getItem('access_token')
    );

    return this.http.get(url + uri, {
      headers,
      observe: 'response'
    });
  }

  sendPOST(url: string, uri: string, param: any, login = false) {

    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers = headers.append(
      "Accept-Language", 'en'
    );
    if (!login) {
      headers = headers.append(
        "Authorization", 'Bearer ' + localStorage.getItem('access_token')
      );
      headers = headers.append(
        "x-ebao-tenant-code", '88insurtech'
      );
    }
    return this.http.post(url + uri, param, {
      headers,
      observe: 'response'
    })
      .pipe(map(data => data));
  }

  sendPUT(url: string, uri: string, param: any) {

    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers = headers.append(
      "Accept-Language", 'en'
    );
    headers = headers.append(
      "Authorization", 'Bearer ' + localStorage.getItem('access_token')
    );

    return this.http.put(url + uri, param, {
      headers,
      observe: 'response'
    })
      .pipe(map(data => data));
  }

  sendDELETE(url: string, uri: string) {

    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers = headers.append(
      "Accept-Language", 'en'
    );
    headers = headers.append(
      "Authorization", 'Bearer ' + localStorage.getItem('access_token')
    );

    return this.http.delete(url + uri)
      .pipe(map(data => data));
  }
}
