import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { error } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class ErrorManagerService {

  constructor(
    private router: Router
  ) { }

  manager(error: any) {
    if (error && error.status) {
      if (error.status === 401) {
        alert('Unauthorized');
        localStorage.removeItem('access_token');
        this.router.navigate(['/home']);
      }
    }
  }
}
