import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgPaymentCardModule } from 'ng-payment-card';
import { MatDialogModule } from '@angular/material/dialog';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './layouts/nav/nav.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { AjudaComponent } from './pages/ajuda/ajuda.component';
import { OuvidoriaComponent } from './pages/ouvidoria/ouvidoria.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { BlogComponent } from './pages/blog/blog.component';
import { PromocoesComponent } from './pages/promocoes/promocoes.component';
import { FormularioComponent } from './pages/ajuda/formulario/formulario.component';
import { FaqComponent } from './pages/ajuda/faq/faq.component';
import { ContatoComponent } from './pages/ajuda/contato/contato.component';
import { LocalizadorComponent } from './pages/ajuda/localizador/localizador.component';
import { ParaVoceComponent } from './pages/ouvidoria/para-voce/para-voce.component';
import { ParaEmpresaComponent } from './pages/ouvidoria/para-empresa/para-empresa.component';
import { NossoTimeComponent } from './pages/sobre/nosso-time/nosso-time.component';
import { TrabalheConoscoComponent } from './pages/sobre/trabalhe-conosco/trabalhe-conosco.component';
import { MidiaComponent } from './pages/sobre/midia/midia.component';
import { SobreNosComponent } from './pages/sobre/sobre-nos/sobre-nos.component';
import { PostComponent } from './pages/blog/post/post.component';
import { ProdutosParaVoceComponent } from './pages/produtos/produtos-para-voce/produtos-para-voce.component';
import { ProdutosParaEmpresaComponent } from './pages/produtos/produtos-para-empresa/produtos-para-empresa.component';
import { VoceUmComponent } from './pages/produtos/produtos-para-voce/voce-um/voce-um.component';
import { VoceDoisComponent } from './pages/produtos/produtos-para-voce/voce-dois/voce-dois.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmpresaDoisComponent } from './pages/produtos/produtos-para-empresa/empresa-dois/empresa-dois.component';
import { EmpresaUmComponent } from './pages/produtos/produtos-para-empresa/empresa-um/empresa-um.component';
import { NomeacoesComponent } from './pages/ajuda/formulario/nomeacoes/nomeacoes.component';
import { ReinvindicacoesComponent } from './pages/ajuda/formulario/reinvindicacoes/reinvindicacoes.component';
import { PagamentosComponent } from './pages/ajuda/formulario/pagamentos/pagamentos.component';
import { FormulariosComponent } from './pages/ajuda/formulario/formularios/formularios.component';
import { LigueComponent } from './pages/ajuda/contato/ligue/ligue.component';
import { EmailComponent } from './pages/ajuda/contato/email/email.component';
import { AgendeComponent } from './pages/ajuda/contato/agende/agende.component';
import { OuvidoriaEmpresaUmComponent } from './pages/ouvidoria/para-empresa/ouvidoria-empresa-um/ouvidoria-empresa-um.component';
import { OuvidoriaEmpresaDoisComponent } from './pages/ouvidoria/para-empresa/ouvidoria-empresa-dois/ouvidoria-empresa-dois.component';
import { TransacionalComponent } from './pages/transacional/transacional.component';
import { ContratarComponent } from './pages/transacional/contratar/contratar.component';
import { CartaoComponent } from './pages/transacional/cartao/cartao.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { BagagemComponent } from './pages/produtos/bagagem/bagagem.component';
import { AssistenciaAutomovelComponent } from './pages/produtos/assistencia-automovel/assistencia-automovel.component';
import { AutomovelFranquiaComponent } from './pages/produtos/automovel-franquia/automovel-franquia.component';
import { ImpedimentoAoTrabalhoComponent } from './pages/produtos/impedimento-ao-trabalho/impedimento-ao-trabalho.component';
import { PremiosComponent } from './pages/home/premios/premios.component';
import { EmpresaTresComponent } from './pages/produtos/produtos-para-empresa/empresa-tres/empresa-tres.component'; 
export let options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    ProdutosComponent,
    AjudaComponent,
    OuvidoriaComponent,
    SobreComponent,
    BlogComponent,
    PromocoesComponent,
    FormularioComponent,
    FaqComponent,
    ContatoComponent,
    LocalizadorComponent,
    ParaVoceComponent,
    ParaEmpresaComponent,
    NossoTimeComponent,
    TrabalheConoscoComponent,
    MidiaComponent,
    SobreNosComponent,
    PostComponent,
    ProdutosParaVoceComponent,
    ProdutosParaEmpresaComponent,
    VoceUmComponent,
    VoceDoisComponent,
    EmpresaDoisComponent,
    EmpresaUmComponent,
    NomeacoesComponent,
    ReinvindicacoesComponent,
    PagamentosComponent,
    FormulariosComponent,
    LigueComponent,
    EmailComponent,
    AgendeComponent,
    OuvidoriaEmpresaUmComponent,
    OuvidoriaEmpresaDoisComponent,
    TransacionalComponent,
    ContratarComponent,
    CartaoComponent,
    BagagemComponent,
    AssistenciaAutomovelComponent,
    AutomovelFranquiaComponent,
    ImpedimentoAoTrabalhoComponent,
    PremiosComponent,
    EmpresaTresComponent,
  ],
  imports: [
    BrowserModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatDialogModule,
    SlickCarouselModule,
    NgPaymentCardModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(options)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
