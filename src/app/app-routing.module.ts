import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { ProdutosParaVoceComponent } from './pages/produtos/produtos-para-voce/produtos-para-voce.component';
import { ProdutosParaEmpresaComponent } from './pages/produtos/produtos-para-empresa/produtos-para-empresa.component';
import { FormularioComponent } from './pages/ajuda/formulario/formulario.component';
import { FaqComponent } from './pages/ajuda/faq/faq.component';
import { ContatoComponent } from './pages/ajuda/contato/contato.component';
import { LocalizadorComponent } from './pages/ajuda/localizador/localizador.component';
import { ParaVoceComponent } from './pages/ouvidoria/para-voce/para-voce.component';
import { ParaEmpresaComponent } from './pages/ouvidoria/para-empresa/para-empresa.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { SobreNosComponent } from './pages/sobre/sobre-nos/sobre-nos.component';
import { NossoTimeComponent } from './pages/sobre/nosso-time/nosso-time.component';
import { TrabalheConoscoComponent } from './pages/sobre/trabalhe-conosco/trabalhe-conosco.component';
import { MidiaComponent } from './pages/sobre/midia/midia.component';
import { BlogComponent } from './pages/blog/blog.component';
import { PostComponent } from './pages/blog/post/post.component';
import { PromocoesComponent } from './pages/promocoes/promocoes.component';
import { VoceUmComponent } from './pages/produtos/produtos-para-voce/voce-um/voce-um.component';
import { VoceDoisComponent } from './pages/produtos/produtos-para-voce/voce-dois/voce-dois.component';
import { EmpresaDoisComponent } from './pages/produtos/produtos-para-empresa/empresa-dois/empresa-dois.component';
import { EmpresaUmComponent } from './pages/produtos/produtos-para-empresa/empresa-um/empresa-um.component';
import { NomeacoesComponent } from './pages/ajuda/formulario/nomeacoes/nomeacoes.component';
import { ReinvindicacoesComponent } from './pages/ajuda/formulario/reinvindicacoes/reinvindicacoes.component';
import { PagamentosComponent } from './pages/ajuda/formulario/pagamentos/pagamentos.component';
import { FormulariosComponent } from './pages/ajuda/formulario/formularios/formularios.component';
import { LigueComponent } from './pages/ajuda/contato/ligue/ligue.component';
import { EmailComponent } from './pages/ajuda/contato/email/email.component';
import { AgendeComponent } from './pages/ajuda/contato/agende/agende.component';
import { OuvidoriaEmpresaUmComponent } from './pages/ouvidoria/para-empresa/ouvidoria-empresa-um/ouvidoria-empresa-um.component';
import { OuvidoriaEmpresaDoisComponent } from './pages/ouvidoria/para-empresa/ouvidoria-empresa-dois/ouvidoria-empresa-dois.component';
import { TransacionalComponent } from './pages/transacional/transacional.component';
import { ContratarComponent } from './pages/transacional/contratar/contratar.component';
import { CartaoComponent } from './pages/transacional/cartao/cartao.component';
import { BagagemComponent } from './pages/produtos/bagagem/bagagem.component';
import { AssistenciaAutomovelComponent } from './pages/produtos/assistencia-automovel/assistencia-automovel.component';
import { AutomovelFranquiaComponent } from './pages/produtos/automovel-franquia/automovel-franquia.component';
import { ImpedimentoAoTrabalhoComponent } from './pages/produtos/impedimento-ao-trabalho/impedimento-ao-trabalho.component';
import { EmpresaTresComponent } from './pages/produtos/produtos-para-empresa/empresa-tres/empresa-tres.component';


const routes: Routes = [

  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  {
    path: 'produtos-voce', component: ProdutosParaVoceComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'voce-um' },
      { path: 'voce-um', component: VoceUmComponent },
      { path: 'voce-dois', component: VoceDoisComponent }
    ],
  },
  {
    path: 'produtos-empresa', component: ProdutosParaEmpresaComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'empresa-um' },
      { path: 'empresa-um', component: EmpresaUmComponent },
      { path: 'empresa-dois', component: EmpresaDoisComponent },
      { path: 'empresa-tres', component: EmpresaTresComponent, outlet: 'mensal' }
    ],
  },
  // {
  //   path: 'produtos-empresa', component: ProdutosParaEmpresaComponent,,
  //   children: [
  //     { path: '', pathMatch: 'full', redirectTo: 'empresa-tres' },
  //   ],
  // },
  { path: 'bagagem', component: BagagemComponent },
  { path: 'assistencia-automovel', component: AssistenciaAutomovelComponent },
  { path: 'automovel-franquia', component: AutomovelFranquiaComponent },
  { path: 'impedimento-ao-trabalho', component: ImpedimentoAoTrabalhoComponent },
  {
    path: 'formulario', component: FormularioComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'formularios' },
      { path: 'nomeacoes', component: NomeacoesComponent },
      { path: 'reinvindicacoes', component: ReinvindicacoesComponent },
      { path: 'pagamentos', component: PagamentosComponent },
      { path: 'formularios', component: FormulariosComponent }
    ],
  },
  { path: 'faq', component: FaqComponent },
  {
    path: 'contato', component: ContatoComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'ligue' },
      { path: 'ligue', component: LigueComponent },
      { path: 'email', component: EmailComponent },
      { path: 'agende', component: AgendeComponent }
    ],
  },
  { path: 'localizador', component: LocalizadorComponent },
  { path: 'ouvidoria-voce', component: ParaVoceComponent },
  {
    path: 'ouvidoria-empresa', component: ParaEmpresaComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'ouvidoria-empresa-um' },
      { path: 'ouvidoria-empresa-um', component: OuvidoriaEmpresaUmComponent },
      { path: 'ouvidoria-empresa-dois', component: OuvidoriaEmpresaDoisComponent }
    ],
  },
  {
    path: 'sobre', component: SobreComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'sobre-nos' },
      { path: 'sobre-nos', component: SobreNosComponent },
      { path: 'nosso-time', component: NossoTimeComponent },
      { path: 'trabalhe-conosco', component: TrabalheConoscoComponent },
      { path: 'midias', component: MidiaComponent }
    ],
  },
  { path: 'blog', component: BlogComponent },
  { path: 'post', component: PostComponent },
  { path: 'promocoes', component: PromocoesComponent },
  { path: 'transacional', component: TransacionalComponent },
  { path: 'contratar', component: ContratarComponent },
  { path: 'cartao', component: CartaoComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
