import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-midia',
  templateUrl: './midia.component.html',
  styleUrls: ['./midia.component.scss']
})
export class MidiaComponent implements OnInit {

  public midia: any[] = [
    {
      id: 1,
      date: `09, Maio de 2019`,
      title: 'Cuide de quem cuida de você!',
      link: 'https://tinyurl.com/y456qt6t',
      description: `
      “Compartilhar amor é compartilhar proteção. Compartilhar proteção é compartilhar a 88i”. 
      Com apenas um clique, você pode contratar um seguro contra roubo e furto para o celular da sua mãe, 
      garantindo proteção em menos de três minutos, sem burocracia
      `
    },
    {
      id: 2,
      date: `08, Maio de 2019`,
      title: 'Projetos de Inovação em Seguros disputam 30 mil euros',
      link: 'https://tinyurl.com/y6z9ambt',
      description: `
      Três propostas brasileiras que prometem gerar impactos sociais positivos para o mercado de seguros 
      estão entre as semifinalistas da segunda edição dos Prêmios Fundación MAPFRE à Inovação Social. 
      A iniciativa global vai premiar com 30 mil euros um projeto de Inovação em Seguros
      `
    },
    {
      id: 3,
      date: `08, Maio de 2019`,
      title: 'Projetos de Inovação em Seguros disputam Prêmio MAPFRE',
      link: 'https://tinyurl.com/y565q7cc',
      description: `
      Uma das iniciativas, o 88Insurtech é uma plataforma digital que pretende revolucionar o mercado de proteção.
      Ela funciona também como um mercado de serviços de tecnologia de proteção mútua, permitindo prestadores de
      serviços ofereçam seus serviços, integrando todo o ecossistema de seguros
      `
    },
    {
      id: 4,
      date: `08, Maio de 2019`,
      title: 'Futurecom reúne players de seguros no maior evento de transformação digital da América Latina',
      link: 'https://tinyurl.com/yygzqeff',
      description: `
      A grande temática foi “Tecnologia e Negócios Disruptivos”, com uma reflexão sobre experiências imersivas, modelos as a service,
      cybersecurity, big data, analytics e Blockchain.
      `
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
