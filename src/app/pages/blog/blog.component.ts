import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  public post: any[] = [
    {
      id: 1,
      img: './assets/photos/Blog_20180304.jpg',
      date: `04, Março de 2018`,
      title: 'Startup traz blockchain para reinventar e ampliar acesso a seguros',
      link: 'https://www.conexaofintech.com.br/insurtech/blockchain-para-ampliar-acesso-a-seguros/',
      description: `
      Startup traz Blockchain para reinventar e ampliar acesso a seguros
      Nos últimos anos, as fintechs têm causado um alvoroço no mercado de
      serviços financeiros brasileiros, mas especialistas em inovação acreditam
      que insurtechs terão um impacto ainda maior de disrupção no Brasil 
      `
    },
    {
      id: 2,
      img: './assets/photos/Blog_20191115.jpg',
      date: `15, Novembro de 2019`,
      title: 'A startup brasileira 88i InsurTech se inscreve no eBaoCloud para acelerar seu crescimento e inovações',
      link: 'https://exame.abril.com.br/negocios/releases/a-startup-brasileira-88i-insurtech-se-inscreve-no-ebaocloud-para-acelerar-seu-crescimento-e-inovacoes/',
      description: `
      A eBaoTech, fornecedora líder global de soluções de seguros digitais, anunciou recentemente que a startup
      InsurTech 88InsurTech (88i), com sede no Brasil, se inscreveu no eBaoCloud® InsureMO®, uma plataforma de
      seguros PaaS (plataforma como serviço), para acelerar seu crescimento e inovações. 
      `
    },
    {
      id: 3,
      img: './assets/photos/Blog_20190424.png',
      date: `23, Junho de 2019`,
      title: '88 Insurtech quer democratizar acesso a seguros com a blockchain',
      link: 'https://panoramacrypto.com.br/88-insurtech-quer-democratizar-acesso-a-seguros-com-a-blockchain/',
      description: `
      Plataforma de seguros promete democratizar o mercado de seguros, proteção e assistência
      com o uso da tecnologia blockchain
      `
    },
    {
      id: 4,
      img: './assets/photos/Blog_20181206.jpg',
      date: `06, Dezembro de 2018`,
      title: '88 Insurtechs mostra como usar blockchain em seguros',
      link: 'https://www.startse.com/noticia/startups/88-insurtechs-startup-que-traz-protecao-do-blockchain-para-os-seguros',
      description: `
      A plataforma utilizada pela startup permite uma maior personalização dos seguros e a 
      redução de custos, oferecendo melhores experiências aos clientes 
      `
    },
    {
      id: 5,
      img: './assets/photos/Blog_20180905.jpg',
      date: `05, Setembro de 2018`,
      title: 'Blockchain provocará tsunami no setor de seguros',
      link: 'http://www.revistacobertura.com.br/2018/09/05/blockchain-provocara-tsunami-no-setor-de-seguros/',
      description: `
      Especialistas reunidos no evento da APTS e ENS analisaram os impactos dessa tecnologia no seguro,
      destacando inovações como a regulação de sinistros em tempo real e a imediata indenização 
      `
    },
    {
      id: 6,
      img: './assets/photos/Blog_Santander.jpg',
      date: `20, Fevereiro de 2020`,
      title: '6 insurtechs brasileiras que estão dominando o mercado',
      link: 'https://santandernegocioseempresas.com.br/app/empreendedorismo/insurtechs',
      description: `
      No mundo moderno, movido pela inovação, era apenas questão de tempo até a tecnologia dominar áreas antes esquecidas,
      como o ramo de seguros. As insurtechs — termo que mistura insurance (seguro) e technology (tecnologia) — são startups
      que revolucionaram o mercado conservador, desburocratizando a contratação de planos de seguro de vida, de imóveis, veículos e muito mais
      `
    },
    {
      id: 7,
      img: './assets/photos/Blog_20190131.jpg',
      date: `31, Janeiro de 2019`,
      title: 'Insurtech brasileira é selecionada em programa internacional para ajudar a transformar positivamente o futuro da humanidade',
      link: 'https://www.cqcs.com.br/noticia/insurtech-brasileira-e-selecionada-em-programa-internacional-para-ajudar-transformar-positivamente-o-futuro-da-humanidade/',
      description: `
      Em entrevista exclusiva ao CQCS, o fundador e CEO da 88 Insurtech, Rodrigo Ventura, conta sua experiência  logo após receber o comunicado
      de que a empresa foi selecionada pela singularity university (universidade concentrada no campo da Nasa e que tem conexões em 133 países e
        já captou centenas de milhões de dólares em investimento para suas startups) para participar do Global Startup Program (GSP) que reúne
        empreendedores de todo o mundo para um trabalho de campo intensivo em vários locais, por meio de uma plataforma de conectividade de ponta
      `
    },
    {
      id: 8,
      img: './assets/photos/Blog_20190614.jpg',
      date: `14, Junho de 2019`,
      title: 'CQCS Insurtech Challenge anuncia o vencedor da maior competição de insurtechs da internet brasileira',
      link: 'https://cqcsinsurtech.com.br/2019/06/14/2240/',
      description: `
      A vencedora da primeira edição do CQCS Insurtech Challenge, a maior competição de insurtechs da internet já realizada no Brasil,
      foi a 88 InsurTech. A decisão foi anunciada nesta quinta-feira, 13 de junho, durante o CQCS Insurtech & Inovação. A disputa conferiu visibilidade
      à todas as participantes do torneio, ao apresentar seus negócios para investidores, stakeholders e CEOs da indústria do seguro
      `
    },
    {
      id: 9,
      img: './assets/photos/Blog_20191027.jpg',
      date: `27, Novembro de 2019`,
      title: 'Confederação Nacional das Seguradoras do Brasil debate uso de Blockchain para seguros',
      link: 'https://mytudocrypto.com/confederacao-nacional-das-seguradoras-do-brasil-debate-uso-de-blockchain-para-seguros/',
      description: `
      A Confederação Nacional das Seguradoras (CNseg) irá debater os impactos da tecnologia blockchain no setor de seguros e os desafios regulatórios
      que envolvem a adoção da tecnologia durante o 13º Insurance Service Meeting, que será realizado em São Paulo, nos dias 6 e 7 de novembro,
      conforme comunicado de imprensa compartilhado com o Cointelegraph em 27 de outubro
      `
    },
    {
      id: 10,
      img: './assets/photos/Blog_20190424.png',
      date: `25, Setembro de 2018`,
      title: 'Startup brasileira é cadastrada entre os fornecedores da ONU',
      link: 'https://www.criptofacil.com/startup-brasileira-e-cadastrada-entre-os-fornecedores-da-onu/',
      description: `
      Recentemente, a 88 InsurTech, startup comandada por Rodrigo Ventura e que busca trazer as inovações e as qualidades da tecnologia blockchain para
      o setor de seguros, firmou uma importante parceria e foi selecionada e cadastrada pela ONU como um dos fornecedores aprovados pela UNICEF, o braço
      de educação das Nações Unidas
      `
    },
    {
      id: 11,
      img: './assets/photos/Blog_20200125.jpg',
      date: `25, Janeiro de 2020`,
      title: 'Rodrigo Ventura: como o blockchain vai mudar completamente o rumo da indústria mundial de seguros',
      link: 'https://www.moneytimes.com.br/rodrigo-ventura-como-o-blockchain-vai-mudar-completamente-o-rumo-da-industria-mundial-de-seguros/',
      description: `
      As regras tradicionais para o mercado de seguros, como Solvência II, Sarbanes Oxley, Basileia e IFRS, passam a ser públicas. Isso muda o jogo
      `
    },
    {
      id: 12,
      img: './assets/photos/Blog_20191027.jpg',
      date: `27, Outubro de 2019`,
      title: 'Confederação Nacional das Seguradoras do Brasil debate uso de Blockchain para seguros',
      link: 'https://cointelegraph.com.br/news/confederacao-nacional-das-seguradoras-do-brasil-debate-uso-de-blockchain-para-seguros',
      description: `
      A Confederação Nacional das Seguradoras (CNseg) irá debater os impactos da tecnologia blockchain no setor de seguros e os desafios regulatórios que
      envolvem a adoção da tecnologia durante o 13º Insurance Service Meeting, que será realizado em São Paulo, nos dias 6 e 7 de novembro, conforme comunicado
      de imprensa compartilhado com o Cointelegraph em 27 de outubro
      `
    },
    {
      id: 13,
      img: './assets/photos/Blog_20190429.jpg',
      date: `29, Abril de 2019`,
      title: 'Soluções do jeito que você e sua empresa precisam',
      link: 'https://www.mondial-assistance.com.br/institucional/2019/04/29/maior-evento-sobre-insurtechs-da-america-latina-reune-grandes-especialistas-em-sp-128/',
      description: `
      MAIOR EVENTO SOBRE INSURTECHS DA AMÉRICA LATINA REÚNE GRANDES ESPECIALISTAS EM SP 128
      `
    },
    {
      id: 14,
      img: './assets/photos/Blog_20190614_2.jpg',
      date: `28, Julho de 2019`,
      title: 'BitSampa vai reunir grandes especialistas em criptomoedas',
      link: 'https://webitcoin.com.br/bitsampa-vai-reunir-grandes-especialistas-em-criptomoedas-jul-28/',
      description: `
      Brasil vai sediar mais um importante evento sobre o mercado crypto na próxima semana. O Bitsampa acontece no dia 31 de agosto, no Hotel Pestana, na capital paulista. 
      O tema principal escolhido para o evento foi Criptomoedas, Blockchain e Liberdade. A programação de palestras começa às 8h30 e vai até às 19h30
      `
    },
    {
      id: 15,
      img: './assets/photos/Blog_20191028.png',
      date: `28, Outubro de 2019`,
      title: 'Futurecom reúne principais players da indústria',
      link: 'https://polinize.com/futurecom-reune-principais-players-da-industria-na-segunda-reuniao-tematica-do-maior-evento-de-transformacao-digital-da-america-latina/',
      description: `
      Futurecom reúne principais players da indústria na segunda reunião temática do maior evento de transformação digital da América Latina
      `
    },
    {
      id: 16,
      img: './assets/photos/Blog_20190424.png',
      date: `20, Outubro de 2019`,
      title: 'Insurtechs: saibam como funcionam as novas seguradoras do mercado',
      link: 'https://fusoesaquisicoes.blogspot.com/2019/10/insurtechs-saibam-como-funcionam-as.html',
      description: `
      Insurtech é a mistura de insurance (seguros, em inglês) com tech, de technology (tecnologia, em inglês). Da mesma forma que as fintechs,
      que atuam no setor financeiro, as novas startups têm surgido no mercado de seguros nos últimos anos e oferecem seguros de saúde, de celulares
      e para passageiros e motoristas de aplicativos. Algumas apólices, de microseguro, custam R$ 5,00.
      `
    },
    {
      id: 17,
      img: './assets/photos/Blog_201191001.jpg',
      date: `01, Outubro de 2019`,
      title: 'Foco deixa de ser veículos e passa a ser proteger pessoas que se movimentam por diferentes modais',
      link: 'https://www.sonhoseguro.com.br/2019/10/foco-deixa-de-ser-veiculos-e-passa-a-ser-proteger-pessoas-que-se-movimentam-por-diferentes-modais/',
      description: `
      Mobilidade e seguros foi o tema debatido no HDI Talks realizado no Distrito Fintech, na cidade de São Paulo, na última segunda-feira
      `
    },
    {
      id: 18,
      img: './assets/photos/Blog_201190619.jpg',
      date: `19, Junho de 2019`,
      title: 'NEW BLOCKCHAIN ACCELERATOR IN CRYPTO VALLEY BLOCKCHAIN PROPULSION',
      link: 'https://www.ico.li/new-blockchain-accelerator-in-crypto-valley/',
      description: `
      A team of blockchain experts and entrepreneurs has launched a blockchain accelerator program in Crypto Valley.
      The goal is to provide expertise and access to a global network.
      `
    },
    {
      id: 19,
      img: './assets/photos/Blog_20181127.jpg',
      date: `27, Novembro de 2018`,
      title: 'Fundador da IOTA fala pela primeira vez em evento na BlockCrypto Conference',
      link: 'https://blockcrypto.com.br/fundador-da-iota-fala-pela-primeira-vez-em-evento-na-blockcrypto-conference/',
      description: `
      A BlockCrypto Conference, evento que ocorreu em São Paulo, nesta semana, nos dia 25 e 26, trouxe desenvolvedores e participantes do
      ecossitema de criptomoedas e blockchain para palestrar e apresentar o que há de mais recente nos desenvolvimentos do setor
      `
    },
    {
      id: 20,
      img: './assets/photos/Blog_20190424.png',
      date: `04, Outubro de 2019`,
      title: 'Operadoras e prestadores de saúde discutiram temas no Encontro ANS',
      link: 'https://monitordigital.com.br/operadoras-e-prestadores-de-saude-discutiram-temas-no-encontro-ans',
      description: `
      A Agência Nacional de Saúde Suplementar (ANS) realizou esta semana, em São Paulo, o Encontro ANS, oportunidade em que reúne os agentes
       do setor para debater temas relacionados ao mercado de planos de saúde e prestar esclarecimentos sobre questões regulatórias.
      `
    },
    {
      id: 21,
      img: './assets/photos/Blog_201190921.jpg',
      date: `21, Fevereiro de 2019`,
      title: 'A Conferência Blockcrypto foi um momento de rencontros.',
      link: 'http://liechtenstein-hub-brazil.com.br/blog/blockcrypto-encontro-gigantes',
      description: `
      Ventura e Borges tem levado conhecimento e levantado a bandeira das criptomoedas tanto a nível nacional, quanto internacional. 
      Em uma conversa um pouco menos formal, o quarteto falou sobre o segmento de Cripto, Fintechs, investimentos, entre outros assuntos
      `
    },
    {
      id: 22,
      img: './assets/photos/Blog_20190614_2.jpg',
      date: `24, Junho de 2019`,
      title: 'Blockchain: como esta tecnologia está mudando os seguros',
      link: 'https://insurance.gr1d.io/trends/post/blockchain-como-esta-tecnologia-esta-mudando-os-seguros-64c88520b1',
      description: `
      O poder da transformação digital e suas tecnologias inovadoras têm sido discutidas incansavelmente em inúmeras publicações todos os dias.
      Porém uma delas, talvez a mais disruptiva de todas, vem causando mudanças significativas na internet como a conhecemos hoje, a blockchain
      `
    },
    {
      id: 23,
      img: './assets/photos/Blog_20190424.png',
      date: `27, Junho de 2019`,
      title: 'A 88I É UM APP DE SERVIÇOS DIGITAIS PARA A CONTRATAÇÃO DE SEGUROS E ASSISTÊNCIAS',
      link: 'https://www.projetodraft.com/a-88i-e-um-app-de-servicos-digitais-para-contratacao-de-seguros-e-assistencias/',
      description: `
      A 88i tem escritório no coworking do InovaBra, em São Paulo, e mais de 1 200 usuários cadastrados. Além de operar no Brasil, a empresa também 
      está constituída na Estônia e na Suíça
      `
    },
    {
      id: 24,
      img: './assets/photos/Blog_20190504.jpg',
      date: `04, Maio de 2019`,
      title: '88i chega para reinventar o mercado de seguros',
      link: 'https://www.conexaofintech.com.br/insurtech/88i-chega-para-reinventar-o-mercado-de-seguros/',
      description: `
      A 88i.com.br, plataforma de serviços digitais para distribuição de seguros, surgiu para oferecer uma nova experiência na contratação de proteção, 
      seguros e assistências, e democratizar o mercado de seguros, promovendo a inclusão social
      `
    },
    {
      id: 25,
      img: './assets/photos/Blog_20190617.jpg',
      date: `17, Junho de 2019`,
      title: '88i vence a maior competição de insurtechs da internet já realizada no Brasil',
      link: 'https://www.cqcs.com.br/noticia/88i-vence-maior-competicao-de-insurtechs-da-internet-ja-realizada-no-brasil/',
      description: `
      A 88i, plataforma de serviços digitais para a distribuição de seguros, foi a grande vencedora do CQCS Insurtech Challenge
      `
    },
    {
      id: 26,
      img: './assets/photos/Blog_20190614_2.jpg',
      date: `14, Junho de 2019`,
      title: 'Insurtech & Inovação: 88i é a vencedora do CQCS Insurtech Challenge',
      link: 'https://www.revistaapolice.com.br/2019/06/88i-e-a-vencedora-do-cqcs-insurtech-challenge/',
      description: `
      A startup de seguros irá representar o Brasil e todas as insurtechs da América Latina em Las Vegas (EUA) durante o maior evento de seguros e tecnologia do mundo, o Insurtech Connect
      `
    },
    {
      id: 27,
      img: './assets/photos/youtube.png',
      date: `22, Julho de 2019`,
      title: 'Blockcrypto expo 88i insurtech Rodrigo Ventuta da 88i seguros em blockchain',
      link: 'https://www.youtube.com/watch?v=Q6zPzNfGk3Q',
      description: `
      Entrevistei o fundador da empresa 88 insurtech Rodrigo Ventura , plataforma de seguros que utiliza a tecnologia Blockchain voltado para o mercado de seguros !!
      Reduçao no preço que pode chegar em ate  30 % .
      `
    },
    {
      id: 28,
      img: './assets/photos/youtube.png',
      date: `24, Setembro de 2019`,
      title: '88i / 88 InsurTech A FORMA MAIS SEGURA, RÁPIDA e CONFIÁVEL para proteger o seu CELULAR e TABLET',
      link: 'https://www.youtube.com/watch?v=qu-Zqfb9dVY',
      description: `
      88i / 88 InsurTech A FORMA MAIS SEGURA, RÁPIDA e CONFIÁVEL para proteger o seu CELULAR e TABLET.
      O modo mais facil e barato de proteger seu celular pagando com cripto !!!
      `
    },
    {
      id: 29,
      img: './assets/photos/Blog_20190424.png',
      date: `02, Abril de 2019`,
      title: 'Após sofrer acidente, empreendedor cria seguradora 100% digital',
      link: 'https://revistapegn.globo.com/Startups/noticia/2019/04/apos-sofrer-acidente-empreendedor-cria-seguradora-100-digital.html',
      description: `
      O economista Rodrigo Ventura nunca pensou que iria trabalhar no mercado de seguros.
      `
    },
    {
      id: 30,
      img: './assets/photos/Blog_20200206.jpg',
      date: `06, Junho de 2019`,
      title: '88i fecha parceria com aplicativo Vá de Táxi para seguro de celulares de taxistas',
      link: 'https://www.segs.com.br/seguros/175146-88i-fecha-parceria-com-aplicativo-va-de-taxi-para-seguro-de-celulares-de-taxistas',
      description: `
      88i, plataforma de serviços digitais para distribuição de seguros de celular e acidentes pessoais, fechou uma parceria com o aplicativo Vá de Táxi, 
      para garantir aos taxistas credenciados o trabalho com maior tranquilidade
      `
    },
    {
      id: 31,
      img: './assets/photos/Blog_20190424.png',
      date: `06, Junho de 2019`,
      title: '88i fecha parceria com aplicativo Vá de Táxi para seguro de celulares de taxistas',
      link: 'https://adnews.com.br/adnegocios/88i-fecha-parceria-com-aplicativo-va-de-taxi-para-seguro-de-celulares-de-taxistas/',
      description: `
      88i, plataforma de serviços digitais para distribuição de seguros de celular e acidentes pessoais, fechou uma parceria com o aplicativo Vá de Táxi, 
      para garantir aos taxistas credenciados o trabalho com maior tranquilidade
      `
    },
    {
      id: 32,
      img: './assets/photos/Blog_20190424.png',
      date: `07, Setembro de 2019`,
      title: 'A 88i de Rodrigo Ventura: nasce um gigante',
      link: 'http://liechtenstein-hub-brazil.com.br/blog/perfil',
      description: `
      Reconhecida nacionalmente como um app de serviços digitais para contratação de seguros e assistências baseado em blockchain, a 88i tem um forte legado, 
      principalmente pelo brilhante trabalho de seu fundador, o economista Rodrigo Ventura
      `
    },
    {
      id: 33,
      img: './assets/photos/Blog_20190424.png',
      date: `28, Junho de 2019`,
      title: 'A 88I É UM APP DE SERVIÇOS DIGITAIS PARA A CONTRATAÇÃO DE SEGUROS E ASSISTÊNCIAS',
      link: 'http://www.futuroprevidencia.com.br/Noticias/a-88i-e-um-app-de-servicos-digitais-para-a-contratacao-de-seguros-e-assistencias',
      description: `
      Segundo o fundador, com apenas um clique e em menos de três minutos, o usuário consegue contratar um seguro. “As novas apólices são auditadas a cada nova compra, 
      o que garante confiabilidade, automação dos processos e segurança da informação.
      `
    },
    {
      id: 34,
      img: './assets/photos/Blog_20190424.png',
      date: `24, Abril de 2019`,
      title: 'A 88i participa da terceira edição do Insurtech Brasil',
      link: 'https://paticionunes.blogspot.com/2019/04/88i-marca-presenca-na-3-edicao-do.html',
      description: `
      A 88i, plataforma de serviços digitais para distribuição de seguros, participa da terceira edição do Insurtech Brasil, no dia 25 de abril, em São Paulo. 
      Rodrigo Ventura, CEO e fundador da 88i, irá apresentar o case inovador da empresa, que conta com uma proposta revolucionária da plataforma no mercado de seguros.
      `
    },
    {
      id: 35,
      img: './assets/photos/Blog_20190522.jpg',
      date: `22, Maio de 2019`,
      title: '88i apresenta sua plataforma na Fintech Conference 2019',
      link: 'https://mgapress.com.br/88i-apresenta-sua-plataforma-na-fintech-conference-2019/',
      description: `
      A 88i, plataforma de serviços digitais para distribuição de seguros, participa da Fintech Conference, no dia 22 de maio, em São Paulo
      `
    },
    {
      id: 36,
      img: './assets/photos/Blog_20190406.png',
      date: `06, Abril de 2019`,
      title: '88i marca presença na 4ª edição do Bitcoin Summit, em Florianópolis',
      link: 'https://mgapress.com.br/88i-marca-presenca-na-4a-edicao-do-bitcoin-summit-em-florianopolis/',
      description: `
      A 88i, plataforma de serviços digitais para distribuição de seguros, participa da 4ª edição do Bitcoin Summit, no dia 6 de abril, em Florianópolis (SC)
      `
    },
    {
      id: 37,
      img: './assets/photos/Blog_20190201.jpg',
      date: `01, Fevereiro de 2019`,
      title: '88i é aprovada em programa global de startup da Singularity University',
      link: 'https://mgapress.com.br/88i-e-aprovada-em-programa-global-de-startup-da-singularity-university/',
      description: `
      A 88i, plataforma de serviços digitais para seguros e blockchain, foi aprovada no Global Startup Program (GSP) da Singularity University
      `
    },
    {
      id: 38,
      img: './assets/photos/Blog_20190202.png',
      date: `02, Fevereiro de 2019`,
      title: 'Confira 5 dicas para proteger seu celular durante as comemorações do Carnaval',
      link: 'https://mgapress.com.br/confira-5-dicas-para-proteger-seu-celular-durante-as-comemoracoes-do-carnaval/',
      description: `
      Plataforma de contratação de seguros para celular registrou um aumento de 500% após notícias sobre furtos em blocos de Carnaval
      `
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
