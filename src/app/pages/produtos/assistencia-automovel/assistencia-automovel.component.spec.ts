import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssistenciaAutomovelComponent } from './assistencia-automovel.component';

describe('AssistenciaAutomovelComponent', () => {
  let component: AssistenciaAutomovelComponent;
  let fixture: ComponentFixture<AssistenciaAutomovelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssistenciaAutomovelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssistenciaAutomovelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
