import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutosParaVoceComponent } from './produtos-para-voce.component';

describe('ProdutosParaVoceComponent', () => {
  let component: ProdutosParaVoceComponent;
  let fixture: ComponentFixture<ProdutosParaVoceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutosParaVoceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutosParaVoceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
