import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoceDoisComponent } from './voce-dois.component';

describe('VoceDoisComponent', () => {
  let component: VoceDoisComponent;
  let fixture: ComponentFixture<VoceDoisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoceDoisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoceDoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
