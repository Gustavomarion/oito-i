import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoceUmComponent } from './voce-um.component';

describe('VoceUmComponent', () => {
  let component: VoceUmComponent;
  let fixture: ComponentFixture<VoceUmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoceUmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoceUmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
