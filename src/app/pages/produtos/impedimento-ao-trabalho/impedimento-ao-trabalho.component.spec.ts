import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpedimentoAoTrabalhoComponent } from './impedimento-ao-trabalho.component';

describe('ImpedimentoAoTrabalhoComponent', () => {
  let component: ImpedimentoAoTrabalhoComponent;
  let fixture: ComponentFixture<ImpedimentoAoTrabalhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpedimentoAoTrabalhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpedimentoAoTrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
