import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomovelFranquiaComponent } from './automovel-franquia.component';

describe('AutomovelFranquiaComponent', () => {
  let component: AutomovelFranquiaComponent;
  let fixture: ComponentFixture<AutomovelFranquiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomovelFranquiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomovelFranquiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
