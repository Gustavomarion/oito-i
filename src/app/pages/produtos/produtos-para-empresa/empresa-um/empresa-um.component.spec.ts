import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaUmComponent } from './empresa-um.component';

describe('EmpresaUmComponent', () => {
  let component: EmpresaUmComponent;
  let fixture: ComponentFixture<EmpresaUmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaUmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaUmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
