import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaDoisComponent } from './empresa-dois.component';

describe('EmpresaDoisComponent', () => {
  let component: EmpresaDoisComponent;
  let fixture: ComponentFixture<EmpresaDoisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaDoisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaDoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
