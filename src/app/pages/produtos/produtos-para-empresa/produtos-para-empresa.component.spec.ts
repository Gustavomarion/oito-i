import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutosParaEmpresaComponent } from './produtos-para-empresa.component';

describe('ProdutosParaEmpresaComponent', () => {
  let component: ProdutosParaEmpresaComponent;
  let fixture: ComponentFixture<ProdutosParaEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutosParaEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutosParaEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
