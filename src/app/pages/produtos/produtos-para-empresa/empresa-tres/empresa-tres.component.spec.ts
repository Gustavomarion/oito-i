import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaTresComponent } from './empresa-tres.component';

describe('EmpresaTresComponent', () => {
  let component: EmpresaTresComponent;
  let fixture: ComponentFixture<EmpresaTresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaTresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaTresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
