import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/shared/providers/login/login.service';
import { BuyService } from 'src/app/shared/providers/buy/buy.service';
import { Buy } from 'src/app/shared/providers/buy/buy.model';
import { ErrorManagerService } from 'src/app/shared/services/error-manager.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-cartao',
  templateUrl: './cartao.component.html',
  styleUrls: ['./cartao.component.scss']
})
export class CartaoComponent implements OnInit {
  public currentPlan: any;
  public buy: Buy;
  constructor(
    private serviceLogin: LoginService,
    private serviceBuy: BuyService,
    private route: ActivatedRoute,
    private router: Router,
    private errorManager: ErrorManagerService
  ) { }

  ngOnInit() {
    this.serviceLogin.login();
    if (!sessionStorage.getItem("current_plan")) {
      this.router.navigate(['/transaccional']);
    }
    /*this.currentPlan = JSON.parse(this.route.snapshot.paramMap.get('current_plan'));*/
    this.currentPlan = JSON.parse(sessionStorage.getItem("current_plan"));
    console.log(this.currentPlan);
  }

  processPayment(cc: any) {
    const form = cc.ccForm;
    if (form && form.status === 'VALID') {
      this.buy = new Buy();
      this.buy.policyInfo.ProposalDate = this.currentPlan.ProposalDate;
      this.buy.policyInfo.EffectiveDate = this.currentPlan.EffectiveDate;
      this.buy.policyInfo.ExpiryDate = this.currentPlan.ExpiryDate;
      const hostname = window.location.hostname;
      this.buy.policyInfo.AgentCode = hostname;
      this.buy.policyInfo.PolicyCustomerList = this.currentPlan.PolicyCustomerList;
      let birthday = this.currentPlan.PolicyCustomerList[0].DateOfBirth;
      birthday = birthday.split('/');
      this.buy.policyInfo.PolicyCustomerList[0].DateOfBirth = birthday[2] + '-' + birthday[1] + '-' + birthday[0];
      this.buy.policyInfo.PolicyLobList = this.currentPlan.PolicyLobList;
      this.buy.policyInfo.PolicyLobList[0].PolicyRiskList[0].OneYearOlder = "true";
      this.buy.policyInfo.PolicyLobList[0].PolicyRiskList[0].EquipmentType = "1";
      this.buy.policyInfo.PolicyLobList[0].PolicyRiskList[0].voucher = "EBAO-VALID";
      this.buy.paymentInfo = {
        CreditCardNo: form.value.cardNumber,
        CreditCardCvv: form.value.ccv,
        CardholdersExpirationDate: form.value.expirationMonth + form.value.expirationYear,
        CardholdersName: form.value.cardHolder
      }
    }
    this.payment(form);
  }

  payment(form: any) {
    const url = this.serviceBuy.container;
    console.log(JSON.stringify(this.buy));
    this.serviceBuy.sendPOST(url, '88insurtech/blockchain.1.0/buy', this.buy).subscribe((resp: any) => {
      console.log(resp)
      if (resp && resp.body) {
        resp = resp.body;
      }
      if (resp.result) {
        // REDIRECT HERE
        $('div.card.boleto').trigger('click');
        setTimeout(() => {
          this.router.navigate(['/home']);
        }, 5000);
      } else {
        /*form.controls.cardNumber.setValue('');
        form.controls.ccv.setValue('');
        form.controls.expirationMonth.setValue(''); 
        form.controls.expirationYear.setValue('');
        form.controls.cardHolder.setValue('');*/
        alert('Ocorreu algo não previsto!');
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

}
