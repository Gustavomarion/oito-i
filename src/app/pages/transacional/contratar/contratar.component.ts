import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/shared/providers/login/login.service';
import { RequestService } from 'src/app/shared/services/request.service';
import { environment } from 'src/environments/environment';
import { ErrorManagerService } from 'src/app/shared/services/error-manager.service';
import { BoletoService } from 'src/app/shared/providers/boleto/boleto.service';
import { Boleto } from 'src/app/shared/providers/boleto/boleto.model';
import { Buy } from 'src/app/shared/providers/buy/buy.model';
import { BuyService } from 'src/app/shared/providers/buy/buy.service';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contratar',
  templateUrl: './contratar.component.html',
  styleUrls: ['./contratar.component.scss']
})
export class ContratarComponent implements OnInit {
  public currentPlan: any;
  public container: string = environment.container;
  public businessCodeTableValueList: any[];
  public states: any[];
  public cities: any[];
  public phones: any[];
  public boleto: Boleto;
  public showButton = true;
  public buy: Buy;
  public requiredEmail: boolean;
  public requiredInvoiceNumber: boolean;
  public showStep2: boolean = false;
  public showStep3: boolean = false;
  public ngForm: any;
  public firstFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  constructor(
    private serviceLogin: LoginService,
    private request: RequestService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    private router: Router,
    private errorManager: ErrorManagerService,
    private serviceBoleto: BoletoService,
    private serviceBuy: BuyService
  ) {
    this.firstFormGroup = formBuilder.group({
      valor: [''],
      marca: [''],
      modelo: [''],
      imei: [''],
      nota_fiscal: ['']
    });
    this.secondFormGroup = formBuilder.group({
      cep: [''],
      endereco: [''],
      numero: [''],
      complemento: [''],
      bairro: [''], 
      estado: [''],
      cidade: [''],
      data: [''],
      cpf: ['']
    });
  }

  ngOnInit() {
    this.serviceLogin.login();
    /*this.currentPlan = JSON.parse(this.route.snapshot.paramMap.get('current_plan'));*/
    if (!sessionStorage.getItem("current_plan")) {
      this.router.navigate(['/transaccional']);
    }
    this.currentPlan = JSON.parse(sessionStorage.getItem("current_plan"));
    this.getEquip();
    this.getEstate();
    this.cities = [];
    this.requiredEmail = true;
    this.requiredInvoiceNumber = true;
  }

  setRequiredEmail() {
    this.requiredEmail = !this.requiredEmail;
    this.step2Active();
  }

  setRequiredInvoiceNumber() {
    this.requiredInvoiceNumber = !this.requiredInvoiceNumber;
    this.step2Active();
  }

  onSubmit(f: any) {
    if (f.invalid !== true) {
      this.showStep3 = true;
      this.ngForm = f;
    } else {
      alert('form invalid');
    }
  }

  payTarget() {
    if (this.ngForm && this.ngForm.invalid !== true) {
      sessionStorage.setItem("current_plan", JSON.stringify(this.currentPlan));
      /*this.router.navigate(['/cartao', { current_plan: JSON.stringify(this.currentPlan) }]);*/
      this.router.navigate(['/cartao']);
    } else {
      alert('form invalid');
    }
  }

  getEquip() {
    const url = this.container;
    this.businessCodeTableValueList = [];
    this.request.sendGET(url, '88insurtech/1.0/iam88-bff-app/getAllCodeTableValueByName?codeTableName=PEI_EQUIPMENT_TYPE').subscribe((resp) => {
      if (resp && resp.body) {
        const response: any = resp.body;
        this.businessCodeTableValueList = response.BusinessCodeTableValueList;
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

  getZip() {
    const url = this.container;
    const object = {
      "cep": this.currentPlan.PolicyCustomerList[0].PostCode
    }
    this.request.sendPOST(url, '88insurtech/v1/api/brzipcode', object).subscribe((resp) => {
      if (resp && resp.body) {
        const response: any = resp.body;
        if (response.status && response.status === "Failed") {
          alert('invalid CEP')
        } else {
          this.currentPlan.PolicyCustomerList[0].Address = response.logradouro;
          this.currentPlan.PolicyCustomerList[0].Suburb = response.bairro;
          this.currentPlan.PolicyCustomerList[0].City = response.cidade_info.codigo_ibge;
          this.currentPlan.PolicyCustomerList[0].State = response.estado_info.codigo_ibge;
          this.getCities();
        }
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

  getEstate() {
    const url = this.container;
    this.states = [];
    this.request.sendGET(url, '88insurtech/1.0/iam88-bff-app/getAllCodeTableValueByName?codeTableName=ESTADO').subscribe((resp) => {
      if (resp && resp.body) {
        const response: any = resp.body;
        this.states = response.BusinessCodeTableValueList;
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

  getCities() {
    const url = this.container;
    const object = {
      "ESTADO_ID": this.currentPlan.PolicyCustomerList[0].State
    }
    this.request.sendPOST(url, '88insurtech/1.0/iam88-bff-app/getCodeTableValueByNameAndConditions/CIDADE', object).subscribe((resp) => {
      if (resp && resp.body) {
        const response: any = resp.body;
        this.cities = response.BusinessCodeTableValueList;
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

  getModelMovil() {
    const url = this.container;
    const object = {
      "MAKE_ID": +this.currentPlan.PolicyLobList[0].PolicyRiskList[0].Make
    }
    this.request.sendPOST(url, '88insurtech/1.0/iam88-bff-app/getCodeTableValueByNameAndConditions/PEI_MODEL', object).subscribe((resp) => {
      if (resp && resp.body) {
        const response: any = resp.body;
        this.phones = response.BusinessCodeTableValueList;
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

  validateCpf(f: any) {
    const cpf = this.currentPlan.PolicyCustomerList[0].IdNo;
    if (cpf) {
      let numbers, digits, sum, i, result, equalDigits;
      equalDigits = 1;
      if (cpf.length < 11) {
        alert('invalid cpf');
        this.currentPlan.PolicyCustomerList[0].IdNo = null;
        return;
      }

      for (i = 0; i < cpf.length - 1; i++) {
        if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
          equalDigits = 0;
          break;
        }
      }

      if (!equalDigits) {
        numbers = cpf.substring(0, 9);
        digits = cpf.substring(9);
        sum = 0;
        for (i = 10; i > 1; i--) {
          sum += numbers.charAt(10 - i) * i;
        }

        result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

        if (result !== Number(digits.charAt(0))) {
          alert('invalid cpf');
          this.currentPlan.PolicyCustomerList[0].IdNo = null;
          return;
        }
        numbers = cpf.substring(0, 10);
        sum = 0;

        for (i = 11; i > 1; i--) {
          sum += numbers.charAt(11 - i) * i;
        }
        result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

        if (result !== Number(digits.charAt(1))) {
          alert('invalid cpf');
          this.currentPlan.PolicyCustomerList[0].IdNo = null;
          return;
        }

        return null;
      } else {
        alert('invalid cpf');
        this.currentPlan.PolicyCustomerList[0].IdNo = null;
        return;
      }
    }
  }

  download() {
    /*this.showButton = false
    const url = 'https://api.pagar.me/';
    this.boleto = new Boleto();
    this.serviceBoleto.sendPOST(url, '1/transactions?api_key=ak_test_jZPLfCI4esIrIHdzx4Ypb2R7AYN9yT', this.boleto, true).subscribe((resp: any) => {
      if (resp && resp.body) {
        console.log(resp.body);
        this.showButton = true;
        const response = resp.body;
        window.open(response.postback_url);
      }
    }, error => {
      this.errorManager.manager(error);
    });*/
    this.buy = new Buy();
    this.buy.policyInfo.ProposalDate = this.currentPlan.ProposalDate;
    this.buy.policyInfo.EffectiveDate = this.currentPlan.EffectiveDate;
    this.buy.policyInfo.ExpiryDate = this.currentPlan.ExpiryDate;
    const hostname = window.location.hostname;
    this.buy.policyInfo.AgentCode = hostname;
    this.buy.policyInfo.PolicyCustomerList = this.currentPlan.PolicyCustomerList;
    let birthday = this.currentPlan.PolicyCustomerList[0].DateOfBirth;
    birthday = birthday.split('/');
    this.buy.policyInfo.PolicyCustomerList[0].DateOfBirth = birthday[2] + '-' + birthday[1] + '-' + birthday[0];
    this.buy.policyInfo.PolicyLobList = this.currentPlan.PolicyLobList;
    this.buy.policyInfo.PolicyLobList[0].PolicyRiskList[0].OneYearOlder = "true";
    this.buy.policyInfo.PolicyLobList[0].PolicyRiskList[0].EquipmentType = "1";
    this.buy.policyInfo.PolicyLobList[0].PolicyRiskList[0].voucher = "EBAO-VALID";
    this.buy.policyInfo.PolicyPaymentInfoList[0].PayModeCode = "70";
    this.buy.policyInfo.PolicyPaymentInfoList[0].IsInstallment = "N";
    this.showButton = false
    const url = this.serviceBuy.container;
    this.serviceBuy.sendPOST(url, '88insurtech/blockchain.1.0/buy', this.buy).subscribe((resp: any) => {
      if (resp && resp.body) {
        this.showButton = true;
        const response = resp.body;
        window.open(response.postback_url);
      }
    }, error => {
      this.errorManager.manager(error);
    });
  }

  checkDate() {
    let current = this.currentPlan.PolicyCustomerList[0].DateOfBirth;
    current = current.split('/');
    if (current.length !== 3) {
      this.currentPlan.PolicyCustomerList[0].DateOfBirth = null;
    }
    const date = new Date(current[2], current[1], current[0]);
  }

  step2Active(event: any = null) {
    if (this.currentPlan.PolicyLobList[0].PolicyRiskList[0].Make == '' || this.currentPlan.PolicyLobList[0].PolicyRiskList[0].Model == '' || (this.requiredInvoiceNumber && this.currentPlan.PolicyLobList[0].PolicyRiskList[0].InvoiceNumber == '') || (this.requiredInvoiceNumber && !this.currentPlan.PolicyLobList[0].PolicyRiskList[0].InvoiceNumber) || (this.requiredEmail && this.currentPlan.PolicyLobList[0].PolicyRiskList[0].IMEI == '') || (this.requiredEmail && !this.currentPlan.PolicyLobList[0].PolicyRiskList[0].IMEI)) {
      this.showStep2 = false;
    } else {
      this.showStep2 = true;
    }
  }

}
