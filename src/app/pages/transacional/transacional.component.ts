import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/shared/providers/login/login.service';
import { QuoteService } from 'src/app/shared/providers/quote/quote.service';
import { Quick } from 'src/app/shared/providers/quote/quote.model';
import { Router } from '@angular/router';
import { ErrorManagerService } from 'src/app/shared/services/error-manager.service';

@Component({
  selector: 'app-transacional',
  templateUrl: './transacional.component.html',
  styleUrls: ['./transacional.component.scss']
})
export class TransacionalComponent implements OnInit {
  public quick: Quick = new Quick();
  public plan: any[];
  public taixas: any[];
  constructor(
    private serviceLogin: LoginService,
    private serviceQuote: QuoteService,
    private router: Router,
    private errorManager: ErrorManagerService
  ) { }

  async ngOnInit() {
    localStorage.clear();
    sessionStorage.clear();
    await this.serviceLogin.login();
    this.quick = new Quick();
    this.plan = [];
    this.getTaixa();
  }

  getTaixa() {
    let init: number = 600;
    this.taixas = [];
    while (init <= 6000) {
      let object = {
        text: "R$" + (init - 200) + " Até R$" + init,
        value: init
      };
      this.taixas.push(object);
      init = init + 200;
    }
  }

  onSubmit(f: any, plan: any = null) {
    if (f.invalid !== true && plan) {
      this.contract(plan);
    } else {
      alert('form invalid');
    }
  }

  async modelChangedCustomerName(event: any) {
    if (this.quick.PolicyCustomerList[0].CustomerName !== "" && this.quick.PolicyCustomerList[0].Mobile !== "" && this.quick.PolicyCustomerList[0].Email !== "" && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== null && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== "") {
      await this.quickQuote(1);
      await this.quickQuote(2);
    }
  }

  async modelChangedMobile(event: any) {
    if (this.quick.PolicyCustomerList[0].CustomerName !== "" && this.quick.PolicyCustomerList[0].Mobile !== "" && this.quick.PolicyCustomerList[0].Email !== "" && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== null && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== "") {
      await this.quickQuote(1);
      await this.quickQuote(2);
    }
  }

  async modelChangedEmail(event: any) {
    const regMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
      if (this.quick.PolicyCustomerList[0].Email != "" && !regMail.test(this.quick.PolicyCustomerList[0].Email)) {
        this.quick.PolicyCustomerList[0].Email = '';
        alert('email invalid');
        return false;
      }
    if (this.quick.PolicyCustomerList[0].CustomerName !== "" && this.quick.PolicyCustomerList[0].Mobile !== "" && this.quick.PolicyCustomerList[0].Email !== "" && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== null && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== "") {
      await this.quickQuote(1);
      await this.quickQuote(2);
    }
  }

  async modelChangedMarketValue(event: any) {
    if (this.quick.PolicyCustomerList[0].CustomerName !== "" && this.quick.PolicyCustomerList[0].Mobile !== "" && this.quick.PolicyCustomerList[0].Email !== "" && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== null && this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue !== "") {
      await this.quickQuote(1);
      await this.quickQuote(2);
    }
  }

  contract(plan: any) {
    plan.PolicyCustomerList[0].CustomerName = this.quick.PolicyCustomerList[0].CustomerName;
    plan.PolicyCustomerList[0].Mobile = this.quick.PolicyCustomerList[0].Mobile;
    plan.PolicyCustomerList[0].Email = this.quick.PolicyCustomerList[0].Email;
    plan.PolicyLobList[0].PolicyRiskList[0].MarketValue = this.quick.PolicyLobList[0].PolicyRiskList[0].MarketValue;
    sessionStorage.setItem("current_plan", JSON.stringify(plan));
    /*this.router.navigate(['/contratar', { current_plan: btoa(JSON.stringify(plan)) }]);*/
    this.router.navigate(['/contratar']);
  }

  quickQuote(plan: number) {
    let day: any;
    let month: any;
    const today = new Date();
    day = today.getDate();
    if (day < 10) {
      day = '0' + day;
    }
    month = +today.getMonth() + 1;
    if (month < 10) {
      month = '0' + month;
    }
    this.quick.ProposalDate = today.getFullYear() + '-' + month + '-' + day + 'T00:00:00';
    const tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1);
    day = tomorrow.getDate();
    if (day < 10) {
      day = '0' + day;
    }
    month = +tomorrow.getMonth() + 1;
    if (month < 10) {
      month = '0' + month;
    }
    this.quick.EffectiveDate = tomorrow.getFullYear() + '-' + month + '-' + day + 'T00:00:00';
    const nextYear = new Date();
    nextYear.setDate(today.getDate() + 365);
    day = nextYear.getDate();
    if (day < 10) {
      day = '0' + day;
    }
    month = +nextYear.getMonth() + 1;
    if (month < 10) {
      month = '0' + month;
    }
    this.quick.ExpiryDate = nextYear.getFullYear() + '-' + month + '-' + day + 'T00:00:00';
    this.quick.PolicyLobList[0].PolicyRiskList[0].PlanName = plan;
    this.quick.PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList = [{
      ProductElementCode: "C101112",
      ServiceProviderCode: "88i"
    }];
    if (plan === 2) {
      this.quick.PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList.push({
        ProductElementCode: "C101113",
        ServiceProviderCode: "88i"
      });
    }
    const url = this.serviceQuote.container;
    const promise = new Promise((resolve, reject) => {
      this.serviceQuote.sendPOST(url, '88insurtech/1.0/iam88-bff-app/quickQuote', this.quick).subscribe((resp: any) => {
        if (resp && resp.body) {
          resolve(resp);
          this.plan.push(resp.body);
        }
      }, error => {
        reject('');
        this.errorManager.manager(error);
      });
    });
    return promise;
  }

}
