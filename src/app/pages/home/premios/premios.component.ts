import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-premios',
  templateUrl: './premios.component.html',
  styleUrls: ['./premios.component.scss', '../home.component.scss']
})
export class PremiosComponent implements OnInit {

  constructor() { }

  public premio: any[] = [
    {
      id: 1,
      image: './assets/icons/premios.svg',
      description: `UNICEF | International | Seleção Innovation Fund | UNICEF (RFPS-NYH-2018-502785)`,
      date: `28 Fevereiro 2018`
    },
    {
      id: 2,
      image: './assets/icons/premios.svg',
      description: `ONU | Suiça | Genebra | 88 passa a ser listada como fornecedor oficial das Nacões Unidas
      através de seu MarketPlace (oficial supplier  N 551162)`,
      date: `17 Setembro 2018`
    },
    {
      id: 3,
      image: './assets/icons/premios.svg',
      description: `EUA | Flórida | NASA | Startup aprovada  no Global Startup Program da Singularity University
      com propósito de impactar 1 Bilhão de pessoas`,
      date: `31 Janeiro 2019`
    },
    {
      id: 4,
      image: './assets/icons/premios.svg',
      description: `Espanha | Mapfre | 88i é selecionada entre 233 no mundo por Inovação Social pela Fundación Mapfre Spaña`,
      date: `28 Março 2019`
    },
    {
      id: 5,
      image: './assets/icons/premios.svg',
      description: `88i Vencedora do Isurtech Challenge – CQCS Insurtech Inovação – Maior Evento de Inovação em Seguros da América Latina`,
      date: `12 Junho 2019`
    },
    {
      id: 6,
      image: './assets/icons/premios.svg',
      description: `Canadá | Toronto | Única Insurtech do Mundo selecionada e aprovada para o Blockchain Bootcamp Program do FacebookLibra`,
      date: `18 Julho 2019`
    },
    {
      id: 7,
      image: './assets/icons/premios.svg',
      description: `Suíça | Genebra | ITU - International Telecomunications Union | Apresenta o estudo de caso de seguros em blockchain da
      88 Insurtech no FG DLT (Focus Group on Distributed Ledger Technology) - Blockchain`,
      date: `01 Agosto 2019`
    }
  ];

  public activePremio: any = {};

  premioConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: ``,
    prevArrow: ``,
    autoplay: true,
  };
  public clicouNoSlide(slide: any) {
    console.log(slide);
    this.activePremio = slide;
  }


  ngOnInit() {
    this.activePremio = this.premio[0];
  }


  teste() {
    console.log('teste');
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    this.activePremio = this.premio[e.currentSlide];
  }

  beforeChange(e) {
  }
}
