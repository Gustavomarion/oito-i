import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  public media: any[] = [
    {
      id: 1,
      image: './assets/icons/star.svg',
      title: `O atendimento foi excelente`,
      description: `O atendimento foi excelente, desde o atendimento para realizar o contrato
      até o acionamento do mesmo, profissionais competentes que me explicaram
      cada detalhe e não pouparam esforços para me dar um novo aparelho.
      Meus mais sinceros parabéns e obrigada.`,
      name: `Vitória Viana`,
      date: `07/02/2020`
    },
    {
      id: 2,
      image: './assets/icons/star.svg',
      title: `Nunca mais ficarei sem seguro`,
      description: `Fui Assaltado e graças a 88i consegui receber o seguro do meu celular.
      Até fiz um novo seguro para o meu novo aparelho. Nunca mais ficarei sem seguro.
      Obrigado 88Insurtech, especialmente à Rosi do atendimento e operações.`,
      name: `William dos Anjos Silva`,
      date: `03/03/2020`
    },
    {
      id: 3,
      image: './assets/icons/star.svg',
      title: `Atendimento excelente`,
      description: `O atendimento foi excelente, esclareci todas as dúvidas e contratei meu seguro muito rápido.`,
      name: `André Silva`,
      date: `06/03/2020`
    }
  ];

  public premio: any[] = [
    {
      id: 1,
      image: './assets/icons/premios.svg',
      description: `UNICEF | International | Seleção Innovation Fund | UNICEF (RFPS-NYH-2018-502785)`,
      date: `28 Fevereiro 2018`
    },
    {
      id: 2,
      image: './assets/icons/premios.svg',
      description: `ONU | Suiça | Genebra | 88 passa a ser listada como fornecedor oficial das Nacões Unidas
      através de seu MarketPlace (oficial supplier  N 551162)`,
      date: `17 Setembro 2018`
    },
    {
      id: 3,
      image: './assets/icons/premios.svg',
      description: `EUA | Flórida | NASA | Startup aprovada  no Global Startup Program da Singularity University
      com propósito de impactar 1 Bilhão de pessoas`,
      date: `31 Janeiro 2019`
    },
    {
      id: 4,
      image: './assets/icons/premios.svg',
      description: `Espanha | Mapfre | 88i é selecionada entre 233 no mundo por Inovação Social pela Fundación Mapfre Spaña`,
      date: `28 Março 2019`
    },
    {
      id: 5,
      image: './assets/icons/premios.svg',
      description: `88i Vencedora do Isurtech Challenge – CQCS Insurtech Inovação – Maior Evento de Inovação em Seguros da América Latina`,
      date: `12 Junho 2019`
    },
    {
      id: 6,
      image: './assets/icons/premios.svg',
      description: `Canadá | Toronto | Única Insurtech do Mundo selecionada e aprovada para o Blockchain Bootcamp Program do FacebookLibra`,
      date: `18 Julho 2019`
    },
    {
      id: 7,
      image: './assets/icons/premios.svg',
      description: `Suíça | Genebra | ITU - International Telecomunications Union | Apresenta o estudo de caso de seguros em blockchain da
      88 Insurtech no FG DLT (Focus Group on Distributed Ledger Technology) - Blockchain`,
      date: `01 Agosto 2019`
    }
  ];

  public activePremio: any = {};  // atributo

  premiosConfig = {  // atributo
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    nextArrow: ``,
    prevArrow: ``,
    dots: true,
  };

  public activeMedia: any = {};  // atributo

  mediaConfig = {  // atributo
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    dots: true,
  };

  // atributos acima do construtor
  constructor() { }
  // metodos abaixo do construtor


  public clicouNoSlide(slide: any) {  // metodo
    console.log(slide);
    this.activePremio = slide;
  }
  public Slide(slide: any) {  // metodo
    console.log(slide);
    this.activeMedia = slide;
  }

  ngOnInit() {  // metodo
    this.activePremio = this.premio[0];
    this.activeMedia = this.media[0];
  }


  teste() {
    console.log('teste');
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    this.activePremio = this.premio[e.currentSlide];
    this.activeMedia = this.media[e.currentSlide];
  }

  beforeChange(e) {
  }

}
