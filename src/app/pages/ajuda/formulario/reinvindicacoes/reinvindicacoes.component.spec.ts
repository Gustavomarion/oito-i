import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReinvindicacoesComponent } from './reinvindicacoes.component';

describe('ReinvindicacoesComponent', () => {
  let component: ReinvindicacoesComponent;
  let fixture: ComponentFixture<ReinvindicacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReinvindicacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReinvindicacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
