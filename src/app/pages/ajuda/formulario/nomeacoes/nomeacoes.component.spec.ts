import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NomeacoesComponent } from './nomeacoes.component';

describe('NomeacoesComponent', () => {
  let component: NomeacoesComponent;
  let fixture: ComponentFixture<NomeacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NomeacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NomeacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
