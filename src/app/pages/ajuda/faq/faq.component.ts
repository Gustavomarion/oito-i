import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss', '../ajuda.component.scss']
})
export class FaqComponent implements OnInit {

  public pergunta: any[] = [
    {
      id: 1,
      image: './assets/icons/mais.png',
      quest: `Quais são os tipos de seguro que a 88i oferece?`,
      resp: `Nós oferecemos seguro de celular (roubo, furto qualificado) e assistência para danos
      causados por queda acidental e por líquidos.`
    },
    {
      id: 2,
      image: './assets/icons/mais.png',
      quest: `Como faço para comprar meu seguro?`,
      resp: `É bem rápido, você pode comprar através do site https://88i.io; Através dos nossos
      parceiros como a 99 ou baixar o nosso App no Google Play e adquirir a proteção na
      hora. Você só precisa ter em mãos os seus dados pessoais e do seu celular.
      Play Store: https://play.google.com/store/apps/details?id=io.insurtech88i`
    },
    {
      id: 3,
      image: './assets/icons/mais.png',
      quest: `O que acontece após eu adquirir a cobertura?`,
      resp: `Você receberá em seu e-mail cadastrado o nosso Manual do Cliente e sua
      Apólice/Bilhete de Seguro.
      As informações da sua proteção estarão disponíveis em sua conta logada no aplicativo
      88i, no site https://88i.io através do chat e ou WhatsApp +5511978038881. `
    },
    {
      id: 4,
      image: './assets/icons/mais.png',
      quest: `Tem carência?`,
      resp: `Não tem carência, você está protegido a partir do momento em que seu pagamento
      for confirmado =).`
    },
    {
      id: 5,
      image: './assets/icons/mais.png',
      quest: `Posso comprar seguros para outras pessoas?`,
      resp: `Sim, você pode comprar seguros para outras pessoas, mas as informações solicitadas
      deverão ser de quem utiliza o aparelho, preenchendo apenas com os seus dados as
      informações referente ao pagamento, como cartão de crédito, por exemplo.`
    },
    {
      id: 6,
      image: './assets/icons/mais.png',
      quest: `Posso incluir serviços em minha apólice durante a validade da mesma?`,
      resp: `Não é possível alterar os serviços após a contratação, neste caso você poderá solicitar
      cancelamento e contratação de um novo seguro.
      Por exemplo: Se você contratou um seguro apenas para roubo ou furto e quiser
      adicionar a assistência para quebra acidental e líquido, você deverá entrar em 
      contato conosco através do chat do app ou site para cancelar a sua apólice e
      fazer uma nova com roubo, furto e assistência.`
    },
    {
      id: 7,
      image: './assets/icons/mais.png',
      quest: `Quero vender meu celular, eu perco a meu seguro?`,
      resp: `Não, mas o seguro continua sendo válido para o celular vendido. Neste caso você
      deverá entrar em contato conosco através do chat do app ou site para fazer a
      solicitação de cancelamento para emissão de um novo seguro para seu novo celular. `
    },
    {
      id: 8,
      image: './assets/icons/mais.png',
      quest: `Não tenho a nota fiscal do celular, posso contratar o seguro mesmo assim?`,
      resp: `Sim, você pode contratar o seguro, mas em caso de sinistro a nota fiscal será um
      documento obrigatório, sendo assim, aconselhamos a entrar em contato com a loja
      em que adquiriu o celular e solicitar a segunda via de sua nota fiscal.
      Se você comprou o celular de outra pessoa, neste caso é necessário ter o termo de
      doação no seu nome. O termo de doação é um documento onde consta todas as
      informações do aparelho e informa que o aparelho era de uma pessoa e agora está
      sendo doada para outra pessoa. `
    },
    {
      id: 9,
      image: './assets/icons/mais.png',
      quest: `Meu celular foi comprado fora do Brasil, posso comprar meu seguro?`,
      resp: `Sim, porém é necessário que tenha a nota fiscal / recibo de compra do aparelho`
    },
    {
      id: 10,
      image: './assets/icons/mais.png',
      quest: `Qual é a abrangência de cobertura deste seguro?`,
      resp: `Território nacional e internacional`
    },
    {
      id: 11,
      image: './assets/icons/mais.png',
      quest: `Tem seguro para Iphone?`,
      resp: `Sim! Você pode comprar o seguro para o seu Iphone através do site: https://88i.io
      No momento só temos o aplicativo para Android, mas em breve será lançado para IOS.
      Caso tenha interesse, entre em contato conosco através de nosso site ou redes sociais `
    },
    {
      id: 12,
      image: './assets/icons/mais.png',
      quest: `Quais os tipos de eventos que não são cobertos?`,
      resp: `Furto simples ou perda do celular.
      O furto simples é a subtração de objeto sem ameaça, não deixando qualquer indício
      de que esse objeto foi furtado, seus pertences simplesmente desaparecem.
      Reposição de acessórios como: carregadores, fones de ouvido, cartão de memória,
      capinhas, e demais acessórios.`
    },
    {
      id: 13,
      image: './assets/icons/mais.png',
      quest: `Quero cancelar meu seguro. Possui multa?`,
      resp: `Não há multa, nem burocracia. É possível cancelar a qualquer momento sem multa (se
        não houver sinistro) Entre em contato conosco através do chat do app ou do site e
        solicite o seu cancelamento.`
    },
    {
      id: 14,
      image: './assets/icons/mais.png',
      quest: `Qual valor mínimo e valor máximo do celular para contratação do seguro?`,
      resp: `O valor mínimo é de R$600,00 e valor máximo de até R$ 6.000,00.`
    },
    {
      id: 15,
      image: './assets/icons/mais.png',
      quest: `Quais são as formas de pagamento?`,
      resp: `Cartão de crédito e boleto bancário.
      Para pagamentos realizados com cartão de crédito os valores serão mensais,
      em forma de assinatura (como Netflix ou Spotify). Ou seja, todo mês o valor
      será descontado e a sua apólice será mensal.
      Para pagamentos com o cartão 99, o motorista poderá ter o desconto de
      R$10,00 na sua primeira mensalidade.
      Para pagamentos realizados com boleto bancário o valor do seguro anual
      deverá ser pago integralmente.`
    },
    {
      id: 16,
      image: './assets/icons/mais.png',
      quest: `O pagamento é anual ou mensal?`,
      resp: `O pagamento por cartão de crédito é mensal, ou seja, assinatura, e cobrado
      todo mês,
      Lembrando que para pagamentos com o cartão 99, o motorista poderá
      ter o desconto de R$10,00 na sua primeira mensalidade
      O pagamento por boleto é anual.
      Lembrando que se você cancelar a assinatura no meio do mês, será cobrado pelo valor
      integral do mês.`
    },
    {
      id: 17,
      image: './assets/icons/mais.png',
      quest: `Como posso calcular o valor do seguro?`,
      resp: `Você pode fazer direto pelo site https://88i.io ou pelo App 88i.
      Play Store: https://play.google.com/store/apps/details?id=io.insurtech88i`
    },
    {
      id: 18,
      image: './assets/icons/mais.png',
      quest: `O que é franquia, quando e quanto eu pago?`,
      resp: `A franquia é o valor da participação que o protegido paga somente quando há um
      sinistro, ou seja, quando o seu celular foi roubado, furtado ou precisa de assistência
      O valor de nossa franquia é de 25% sobre o valor do celular que foi informado na
      compra do seguro.
      Por exemplo: Se o valor informado na compra do seu seguro é de R$1.000,00 e
      houve um sinistro, você paga 25% (R$250,00) de franquia e a 88i te envia um
      celular do mesmo modelo ou modelo similar em caso de roubo ou furto.
      Em casos de assistência o seu celular é devolvido totalmente reparado e em casos de
      perda total é enviado um celular do mesmo modelo ou similar.
      `
    },
    {
      id: 19,
      image: './assets/icons/mais.png',
      quest: `Qual o valor do meu celular que eu devo informar no site ou no App 88i?`,
      resp: `No site ou no App, para selecionar o valor do celular, os valores estão classificados
      por faixas de R$200 a R$200,00 reais, sendo assim você deve sempre arredondar o
      valor para cima.
      Por exemplo: Se o meu celular custa R$1.700,00 eu devo informar o valor de
      R$1.800,00.
      Caso você tenha um sinistro a reposição do seu celular será baseado no valor
      informado na compra, ou seja, até R$1.800,00 reais.
      `
    },
    {
      id: 20,
      image: './assets/icons/mais.png',
      quest: `Quais bandeira de cartão de crédito são aceitas?`,
      resp: `Visa, Mastercard, American Express, Elo, Hipercard, Diners, Discover, Aura e JCB.`
    },
    {
      id: 21,
      image: './assets/icons/mais.png',
      quest: `Como fico sabendo que meu boleto foi pago e que estou segurado?`,
      resp: `Após a compensação bancária, você será notificado por e-mail e pelo aplicativo com a
      confirmação de pagamento e emissão do bilhete de seguro.`
    },
    {
      id: 22,
      image: './assets/icons/mais.png',
      quest: `Meu boleto venceu, o que eu faço?`,
      resp: `Entre em contato com a 88i através do chat do app, site, WhatsApp +551197803-8881
      ou das nossas redes sociais, informe o vencimento que o mesmo será encaminhado via
      e-mail para você.
      Para motoristas 99, pagando com o boleto anual, há um desconto de R$10,00
      no seguro até a data do vencimento. `
    },
    {
      id: 23,
      image: './assets/icons/mais.png',
      quest: `Onde eu coloco o cupom de referência?`,
      resp: `Após escolher o valor do seu aparelho e o tipo de cobertura desejado, no resumo da
      Proposta, há um campo em amarelo: " VOCÊ TEM UM CÓDIGO PROMOCIONAL?" Basta
      inserir o seu código recebido para validação e pronto.`
    },
    {
      id: 24,
      image: './assets/icons/mais.png',
      quest: `Onde eu posso baixar o app 88i?`,
      resp: `Play store: https://play.google.com/store/apps/details?id=io.insurtech88i`
    },
    {
      id: 25,
      image: './assets/icons/mais.png',
      quest: `Como faço para informar um sinistro ou uma avaria? `,
      resp: `Em casos de Roubo ou furto com celulares que foi comprado até um ano:
      Horário de atendimento: de segunda a sexta-feira, das 8h às 18h (exceto
      feriados)
      Capitais e regiões metropolitanas: 0300 777 8807
      Demais localidades: 0800 777 8003
      Em casos de Roubo ou furto com celulares que foi comprado por mais de um ano e
      Assistências:
      Horário de atendimento: de segunda a sexta-feira, das 8h às 18h (exceto
      feriados)
      Telefone: (11) 97803-8881
      Chat: - Aplicativo ou https://88i.io
      E-mail: suporte@88i.io`
    },
    {
      id: 26,
      image: './assets/icons/mais.png',
      quest: `Quais os documentos que preciso ter para informar um sinistro?`,
      resp: `Em casos de assistência:
      ○ RG e CPF ou CNH
      ○ Comprovante de endereço
      ○ Nota fiscal do Celular
      ○ Relato do ocorrido
      Em casos de Roubo ou furto qualificado:
      ○ RG e CPF ou CNH
      ○ Comprovante de endereço
      ○ Nota fiscal do Celular
      ○ Boletim de Ocorrência
      ○ Bloqueio do IMEI na operadora de telefonia `
    },
    {
      id: 27,
      image: './assets/icons/mais.png',
      quest: `Em quanto tempo eu recebo o meu aparelho?`,
      resp: `Em casos de Roubo ou Furto, o envio de novo celular normalmente ocorre em 7
      dias úteis, mas a seguradora pode eventualmente levar até 30 dias em casos de
      investigação do sinistro. (Após todos os documentos entregues)
      Em casos de assistência são de 7 dias úteis, contando a partir do dia em que o
      celular chegar na assistência. (Após todos os documentos entregues)`
    },
    {
      id: 28,
      image: './assets/icons/mais.png',
      quest: `Em quais situações pode não acontecer o envio de um novo celular ou o conserto
      do celular?`,
      resp: `Em casos de pendência de documentos, informações divergentes,
      Celular com a etiqueta do IMEI de dentro do aparelho avariada, ilegível,
      forjada.
      Constatação de fraude informada pela nossa assistência técnica (Em casos de
      assistência)`
    },
    {
      id: 29,
      image: './assets/icons/mais.png',
      quest: `Quantos sinistros posso ter durante minha vigência?`,
      resp: `Você poderá ter apenas um sinistro por apólice, caso você tenha uma abertura de
      sinistro e ainda queira ser protegido novamente, terá que baixar o App 88i no seu
      celular novo ou restaurado e solicitar um novo seguro pelo app ou pelo site 88i.io `
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
