import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-localizador',
  templateUrl: './localizador.component.html',
  styleUrls: ['./localizador.component.scss', '../ajuda.component.scss']
})
export class LocalizadorComponent implements OnInit {
  public localizacao: any[] = [
    {
      id: 1,
      icon: './assets/icons/localizacao-amarela.png',
      title: 'Endereço 1',
      description: `
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Fusce quam lacus, euismod at felis id, sagittis sollicitudin lacus.
                  `
    },
    {
      id: 2,
      icon: './assets/icons/localizacao-amarela.png',
      title: 'Endereço 2',
      description: `
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Fusce quam lacus, euismod at felis id, sagittis sollicitudin lacus.
                  `
    },
    {
      id: 3,
      icon: './assets/icons/localizacao-amarela.png',
      title: 'Endereço 3',
      description: `
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Fusce quam lacus, euismod at felis id, sagittis sollicitudin lacus.
                  `
    },
    {
      id: 4,
      icon: './assets/icons/localizacao-amarela.png',
      title: 'Endereço 4',
      description: `
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Fusce quam lacus, euismod at felis id, sagittis sollicitudin lacus.
                  `
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
