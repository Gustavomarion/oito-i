import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promocoes',
  templateUrl: './promocoes.component.html',
  styleUrls: ['./promocoes.component.scss']
})
export class PromocoesComponent implements OnInit {

  public promotions: any[] = [
    {
      id: 1,
      title: `Lorem ipsum dolor sit amet`,
      description: `Descubra como receber seu voucher de celular clicando no botão abaixo.`,
      button: `Descubra mais`
    },
    {
      id: 2,
      title: `Lorem ipsum dolor sit amet`,
      description: `Use o voucher 88I e ganhe 15%.`,
      button: `Faça uma cotação
      `
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
