import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OuvidoriaEmpresaDoisComponent } from './ouvidoria-empresa-dois.component';

describe('OuvidoriaEmpresaDoisComponent', () => {
  let component: OuvidoriaEmpresaDoisComponent;
  let fixture: ComponentFixture<OuvidoriaEmpresaDoisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OuvidoriaEmpresaDoisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OuvidoriaEmpresaDoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
