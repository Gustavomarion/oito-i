import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OuvidoriaEmpresaUmComponent } from './ouvidoria-empresa-um.component';

describe('OuvidoriaEmpresaUmComponent', () => {
  let component: OuvidoriaEmpresaUmComponent;
  let fixture: ComponentFixture<OuvidoriaEmpresaUmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OuvidoriaEmpresaUmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OuvidoriaEmpresaUmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
