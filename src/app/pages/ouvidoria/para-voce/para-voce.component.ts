import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-para-voce',
  templateUrl: './para-voce.component.html',
  styleUrls: ['./para-voce.component.scss', '../../ajuda/faq/faq.component.scss']
})
export class ParaVoceComponent implements OnInit {
  public pergunta: any[] = [
    {
      id: 1,
      image: './assets/icons/mais.png',
      quest: `Lorem ipsum dolor sit amet`,
      resp: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Accumsan felis sem vitae vitae. Vehicula rutrum urna, quis donec
      semper amet platea tempus. Diam at non nam quam condimentum
      purus diam sed morbi. Adipiscing donec amet egestas fusce amet
      amet, posuere mauris pulvinar. Volutpat semper eget hendrerit
      vestibulum diam pulvinar vulputate pharetra. Eget proin eget
      scelerisque aliquet.`
    },
    {
      id: 2,
      image: './assets/icons/mais.png',
      quest: `Lorem ipsum dolor amet sit `,
      resp: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Accumsan felis sem vitae vitae. Diam at non nam quam condimentum
      purus diam sed morbi. Adipiscing donec amet egestas fusce amet
      amet, posuere mauris pulvinar. Vehicula rutrum urna, quis donec
      semper amet platea tempus. Volutpat semper eget hendrerit
      vestibulum diam pulvinar vulputate pharetra. Eget proin eget
      scelerisque aliquet.`
    },
    {
      id: 3,
      image: './assets/icons/mais.png',
      quest: `Lorem ipsum sit amet dolor `,
      resp: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Accumsan felis sem vitae vitae. Vehicula rutrum urna, quis donec
      semper amet platea tempus. Eget proin eget
      scelerisque aliquet.Diam at non nam quam condimentum
      purus diam sed morbi. Adipiscing donec amet egestas fusce amet
      amet, posuere mauris pulvinar. Volutpat semper eget hendrerit
      vestibulum diam pulvinar vulputate pharetra`
    },
    {
      id: 4,
      image: './assets/icons/mais.png',
      quest: `Lorem ipsum sit amet dolor`,
      resp: `Volutpat semper eget hendrerit
      vestibulum diam pulvinar vulputate pharetra. Eget proin eget
      scelerisque aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Accumsan felis sem vitae vitae. Vehicula rutrum urna, quis donec
      semper amet platea tempus. Diam at non nam quam condimentum
      purus diam sed morbi. Adipiscing donec amet egestas fusce amet
      amet, posuere mauris pulvinar. .`
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
